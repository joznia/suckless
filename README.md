# suckless

My personal builds of the Suckless software.

To use the various custom scripts that dwmblocks and other programs use, put the `.local` and `.dwm` folders in your home directory, and add `~/.local/bin` to your `PATH`, for example with:

`[ -d "$HOME/.local/bin" ] && PATH="$HOME/.local/bin:$PATH"`

in your `~/.bashrc`.

