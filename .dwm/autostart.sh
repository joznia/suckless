#!/bin/sh
xrandr --output DisplayPort-1 --rate 240 --mode 1920x1080 --rotate normal --left-of DisplayPort-0 --output DisplayPort-0 --primary --mode 1920x1080 --rotate normal --rate 240 --left-of DisplayPort-2 --output DisplayPort-2 --mode 1920x1080 --rotate normal --rate 240 &
picom -b &
~/.fehbg &
xsetroot -cursor_name left_ptr &
disableaccel &
xrdb -merge ~/.Xresources &
lxsession &

dwmblocks &
