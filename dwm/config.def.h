/************************************************************************
 *         _                   _                                        *
 *        (_)___  ____  ____  (_)___ _                                  *
 *       / / __ \/_  / / __ \/ / __ `/                                  *
 *      / / /_/ / / /_/ / / / / /_/ /                                   *  
 *   __/ /\____/ /___/_/ /_/_/\__,_/                                    *
 *  /___/                                                               *
 *                                                                      *
 *  joznia's dwm config                                                 *
 *                                                                      *
 *  not a ton of patches, but looks and feels pretty similar to xmonad  *
 *                                                                      *
 *  be sure to copy the `.dwm' and `.local' folders to $HOME and also   *
 *  install my build of dwmblocks. by default this uses the st terminal *
 *  terminal (also in the suckless repo) but you can easily change it.  *
 *  you also probably want to install `nerd-fonts-complete' from the    *
 *  AUR to get icons in the panel.                                      *
 *                                                                      *
 *  To make new windows appear at master rather than stack, open dwm.c  *
 *  and apply the following diff:                                       *
 *                                                                      *
 *  uncomment line 1163: attachabove(c)                                 *
 *  comment line 1165: attachBelow(c)                                   *
 *  uncomment line 1631: attachabove(c)                                 *
 *  comment line 1633: attachBelow(c)                                   *
 *  uncomment line 2118: attachabove(c)                                 *
 *  comment line 2119: attachBelow(c)                                   *
 *                                                                      *
 ************************************************************************/

/* appearance */
static const unsigned int borderpx  = 2;        /* border pixel of windows */
static const unsigned int gappx     = 6;        /* gaps between windows */
static const unsigned int snap      = 5;        /* snap pixel */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = { "Mononoki Nerd Font:size=11" };
static const char dmenufont[]       = "Mononoki Nerd Font:size=11";
static const char col_dark1[]       = "#282a36";
static const char col_dark2[]       = "#282a36";
static const char col_white[]       = "#f8f8f2";
static const char col_dark4[]       = "#282a36";
static const char col_pink[]        = "#ff79c6";
static const unsigned int baralpha = 0xFF;
static const unsigned int borderalpha = OPAQUE;
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_white, col_dark1, col_dark2 },
	[SchemeSel]  = { col_dark4, col_pink,  col_pink  },
};
static const unsigned int alphas[][3]      = {
       /*               fg      bg        border     */
       [SchemeNorm] = { OPAQUE, baralpha, borderalpha },
       [SchemeSel]  = { OPAQUE, baralpha, borderalpha },
};

/* tagging */
static const char *tags[] = { "    ", "   ", "   ", "   ", "   ", "   ", "   ", "   ", " ﱘ  " };
static const char *alttags[] = { "    ", "   ", "   ", "   ", "   ", "   ", "   ", "   ", " ﱘ  " };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class      instance    title       tags mask     isfloating   monitor */
	{ "Gimp",     NULL,       NULL,       0,            1,           -1 },
};

/* layout(s) */
static const float mfact     = 0.50; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */
static const int attachbelow = 1;    /* 1 means attach after the currently active window */

#include "fibonacci.c"
static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "|  tall ",     tile },    /* first entry is default */
	{ "|  float ",    NULL },    /* no layout function means floating behavior */
	{ "|  mono ",     monocle },
// 	{ "|  spiral ",   spiral },
// 	{ "|  dwindle ",  dwindle },
	{ NULL,       NULL },
};

/* key definitions */
#define ALT Mod1Mask
#define WIN Mod4Mask
#define MOD WIN
#define CTRL ControlMask
#define WINCTRL WIN|CTRL
#define WINALT WIN|ALT
#define ALTSPC ALT|XK_space
#define TAGKEYS(KEY,TAG) \
	{ MOD,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MOD|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MOD|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MOD|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* set terminal */
#define TERMINAL "st"
//#define TERMINAL "alacritty"

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_dark1, "-nf", col_white, "-sb", col_pink, "-sf", col_dark4, NULL };
static const char *termcmd[]  = { TERMINAL, NULL };

#include "movestack.c"
static Key keys[] = {
	/* modifier                     key        function        argument */
	{ MOD|ShiftMask,                XK_Return, spawn,          {.v = dmenucmd } },
	{ MOD,                          XK_Return, spawn,          {.v = termcmd } },
	{ MOD,                          XK_b,      togglebar,      {0} },
	{ MOD,                          XK_j,      focusstack,     {.i = +1 } },
	{ MOD,                          XK_k,      focusstack,     {.i = -1 } },
	{ MOD|ShiftMask,                XK_j,      movestack,      {.i = +1 } },
    { MOD|ShiftMask,                XK_k,      movestack,      {.i = -1 } },
	{ MOD,                          XK_i,      incnmaster,     {.i = +1 } },
	{ MOD,                          XK_d,      incnmaster,     {.i = -1 } },
	{ MOD,                          XK_h,      setmfact,       {.f = -0.05} },
	{ MOD,                          XK_l,      setmfact,       {.f = +0.05} },
	{ MOD,                          XK_Tab,    zoom,           {0} },
/*	{ MOD,                          XK_Tab,    view,           {0} }, */
	{ MOD|ShiftMask,                XK_c,      killclient,     {0} },
	{ MOD,                          XK_space,  cyclelayout,    {.i = +1 } },
	{ MOD|ShiftMask,                XK_f,      togglefloating, {0} },
	{ MOD,                          XK_0,      view,           {.ui = ~0 } },
	{ MOD|ShiftMask,                XK_0,      tag,            {.ui = ~0 } },
	{ MOD,                          XK_w,      focusmon,       {.i = +1 } },
	{ MOD,                          XK_r,      focusmon,       {.i = -1 } },
	{ MOD|ShiftMask,                XK_w,      tagmon,         {.i = +1 } },
	{ MOD|ShiftMask,                XK_r,      tagmon,         {.i = -1 } },
	{ WINCTRL,                      XK_e,      spawn,          SHCMD(TERMINAL " -e nvim")},
	{ WINCTRL,                      XK_m,      spawn,          SHCMD(TERMINAL " -e ncmpcpp")},
	{ WINCTRL,                      XK_w,      spawn,          SHCMD("tabbed surf -e")},
	{ WINALT,                       XK_e,      spawn,          SHCMD("emacsclient -c")},
	{ WINALT,                       XK_v,      spawn,          SHCMD("emacsclient -c --eval \"(vterm_macro)\"")},
	{ WINALT,                       XK_i,      spawn,          SHCMD("emacsclient -c --eval \"(irc_macro)\"")},
	{ WINALT,                       XK_w,      spawn,          SHCMD("emacsclient -c --eval \"(eww_macro)\"")},
	{ WINALT,                       XK_g,      spawn,          SHCMD("emacsclient -c --eval \"(magit-status)\"")},
	{ WINALT,                       XK_s,      spawn,          SHCMD("emacsclient -c --eval \"(eshell)\"")},
	{ WINALT,                       XK_b,      spawn,          SHCMD("emacsclient -c --eval \"(ibuffer)\"")},
	{ WINALT,                       XK_d,      spawn,          SHCMD("emacsclient -c --eval \"(dired nil)\"")},
	{ WINALT,                       XK_m,      spawn,          SHCMD("emacsclient -c --eval \"(mu4e)\"")},
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	{ MOD|ShiftMask,                XK_q,      quit,           {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MOD,            Button1,        movemouse,      {0} },
	{ ClkClientWin,         MOD,            Button2,        togglefloating, {0} },
	{ ClkClientWin,         MOD,            Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MOD,            Button1,        tag,            {0} },
	{ ClkTagBar,            MOD,            Button3,        toggletag,      {0} },
};

