//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/
	{"    ", "memory",            1,          		0},
	{"   ", "freedisk",           1,          		0},
	{"   ", "kernel",             600,        		0},
// 	{"   ", "pacupdate",	       60,         		0},
	{"   ", "paccount",	       10,         		0},
	{" 祥  ", "pcuptime",	       1,          		0},
	{"   ",  "datentime",		   1,          		0},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = "  | ";
static unsigned int delimLen = 5;
